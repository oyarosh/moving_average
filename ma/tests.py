import unittest

from ma.moving_average import MovingAverage


class MovingAverageTest(unittest.TestCase):
    def setUp(self):
        self.date_column = [
            u'Date', u'31.12.09 0:00', u'01.01.10 0:00',
            u'02.01.10 0:00', u'03.01.10 0:00', u'04.01.10 0:00',
            u'05.01.10 0:00', u'06.01.10 0:00', u'07.01.10 0:00', ]
        self.vis_column = [
            u'Visitors', u'234', u'34', u'2', u'21',
            u'6', u'34', u'45', u'8', ]
        self.ma_period = 3

        # https://docs.google.com/spreadsheets/d/1tD-bsL89YL_QoGolSheqamD5YbSnNc54-2oC52jDQTk/edit#gid=0
        self.ssid = '1tD-bsL89YL_QoGolSheqamD5YbSnNc54-2oC52jDQTk'

        self.req_data = {'spreadsheetId': self.ssid, 'range': 'sheet1'}
        self.instance = MovingAverage(ma_period=self.ma_period, ssid=self.ssid)

    def _clear_sheet(self, sheet_id=0):
        requests = []
        requests.append({
            "updateCells": {
                "range": {
                    "sheetId": str(sheet_id)},
                "fields": "userEnteredValue"
            }
        })
        body = {'requests': requests}
        self.instance.get_service().spreadsheets()\
            .batchUpdate(spreadsheetId=self.ssid, body=body).execute()

    def test_calculate_ma_method(self):
        result = self.instance.calculate_ma(self.date_column, self.vis_column)
        self.assertDictEqual(
            result, {1: None, 2: None, 3: None, 4: 90, 5: 19, 6: 9, 7: 20, 8: 28})

        self.vis_column[5] = u'qwerty'
        result = self.instance.calculate_ma(self.date_column, self.vis_column)
        self.assertEqual(result[6], None)
        self.vis_column[5] = None
        with self.assertRaises(TypeError):
            self.instance.calculate_ma(self.date_column, self.vis_column)
        self.vis_column[5] = 6
        result = MovingAverage(ma_period=0)\
            .calculate_ma(self.date_column, self.vis_column)
        self.assertSetEqual(set([None]), set(result.values()))
        result = MovingAverage(ma_period=-3)\
            .calculate_ma(self.date_column, self.vis_column)
        self.assertSetEqual(set([None]), set(result.values()))

    def test_get_columns_number(self):
        result = self.instance.get_columns_number(self.req_data)
        self.assertTupleEqual((0, 1, 2), result)

        self.instance.insert_cell(0, 0, 0, 'Bla-Bla')
        result = self.instance.get_columns_number(self.req_data)
        self.assertEqual(-1, result[0])

    def test_moving_average_column(self):
        result = self.instance.get_columns_number(self.req_data)
        self.assertEqual(2, result[2])

        self.instance.insert_cell(0, 0, 2, 'Bla-Bla')
        result = self.instance.get_columns_number(self.req_data)
        ma_column_num = self.instance.check_ma_column(result[2], 3)
        self.assertEqual(3, ma_column_num)

    def test_process(self):
        result = self.instance.process()
        self.assertEqual('success', result['status'])

    def tearDown(self):
        self._clear_sheet()
        for num, date in enumerate(self.date_column):
            self.instance.insert_cell(0, num, 0, date)
        for num, vis in enumerate(self.vis_column):
            self.instance.insert_cell(0, num, 1, vis)
        self.instance.insert_cell(0, 0, 2, 'Moving Average')
