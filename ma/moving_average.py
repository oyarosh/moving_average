from __future__ import print_function, absolute_import
import logging
import itertools

from googleapiclient.errors import HttpError
from dateutil.parser import parse

from ma.utils import get_service
from settings.base import *

logging.basicConfig(format='>> %(message)s')
logr = logging.getLogger('ma')


class MovingAverage(object):
    """Calculate moving average in google spreadsheet 
        and add it to separate column 'Moving Average'
        on the same sheet

        init params:
            ssid: spreadsheetID (string) (requred)
            ma_period: moving_average period (int) (not requred - default=3)
    """

    def __init__(self, *args, **kwargs):
        self.ssid = kwargs.get('ssid')
        self.ma_period = kwargs.get('ma_period', DEFAULT_MA)
        self.service = get_service()
        self.spr_props = None

    @classmethod
    def get_service(cls):
        return get_service()

    def _is_correct_access(self):
        return_data = {}
        try:
            self.service.spreadsheets().get(
                **{'spreadsheetId': self.ssid}).execute()
        except HttpError as e:
            if e.resp.get('status') == '403':
                return_data['status'] = 'error'
                return_data['message'] = \
                    'You don`t any have permission for this spreadsheet'
            elif e.resp.get('status') == '404':
                return_data['status'] = 'error'
                return_data['message'] = \
                    'Spreadsheet ID didn`t found.'
            else:
                raise HttpError(e.resp, str(e))
            return False, return_data
        else:
            return True, return_data

    def get_columns_number(self, req_data):
        result = self.service.spreadsheets().values().get(**req_data).execute()
        headers = result.get('values', '')[0]
        col_num = lambda x: headers.index(x) if x in headers else -1
        return col_num('Date'), col_num('Visitors'), col_num('Moving Average')

    def check_ma_column(self, ma_column_num, total_column):
        if ma_column_num == -1:
            ma_column_num = total_column
            sheet_id = 0
            if self.spr_props and self.spr_props.get('sheetId', ''):
                sheet_id = self.spr_props.get('sheetId', '')
            self.insert_cell(sheet_id, 0, ma_column_num, 'Moving Average')
        return ma_column_num

    def insert_cell(self, sheet_id, row_index, column_index, value):
        requests = []
        value_type = "stringValue" if isinstance(value, basestring) else "numberValue"
        requests.append({
            "updateCells": {
                "start": {
                    "sheetId": sheet_id,
                    "rowIndex": row_index,
                    "columnIndex": column_index},
                "fields": "userEnteredValue",
                "rows": [{
                    "values": [{
                        "userEnteredValue": {value_type: value},
                    }]
                }],
            }
        })
        body = {
            'requests': requests
        }
        return self.service.spreadsheets().batchUpdate(spreadsheetId=self.ssid, body=body).execute()

    def is_correct_visitors(self, vis):
        try:
            int(vis)
            return True
        except ValueError:
            return False

    def is_correct_date(self, date):
        try:
            parse(date)
            return True
        except ValueError:
            return False

    def is_correct_ma_period(self, ma_period):
        try:
            int(ma_period)
            return True
        except ValueError:
            return False
        if ma_period > 0:
            return True
        else:
            return False

    def calculate_ma(self, date_column, vis_column):
        ma_map = {}
        for date, vis in itertools.izip(date_column[1:], vis_column[1:]):
            key = date_column.index(date)
            if not self.is_correct_visitors(vis):
                print ('incorect visitor data at line #%s' % str(key + 1))
            if not self.is_correct_date(date):
                print ('incorect date data at line #%s' % str(key + 1))
            if not self.is_correct_ma_period(self.ma_period):
                print ('incorect ma_period. ma_period <= 0 or not converable to int')

            vis_period = vis_column[key - self.ma_period:key]
            if len(vis_period) != self.ma_period:
                ma_value = None
                ma_map[key] = ma_value
                continue

            try:
                ma_value = sum(map(lambda x: int(x), vis_period)) / self.ma_period
            except ValueError:
                ma_value = None
            except ZeroDivisionError:
                ma_value = None

            ma_map[key] = ma_value

        return ma_map

    def process(self):
        is_correct, data = self._is_correct_access()
        if not is_correct:
            return data

        req_data = {'spreadsheetId': self.ssid}
        spreadsheet = self.service.spreadsheets().get(**req_data).execute()
        if len(spreadsheet.get('sheets')) != 1:
            logr.info('spreadsheet more than one, choose the first')

        self.spr_props = spreadsheet.get('sheets')[0].get('properties', {})
        req_data['range'] = self.spr_props.get('title', '')

        date_column_num, vis_column_num, ma_column_num = \
            self.get_columns_number(req_data)

        if date_column_num == -1:
            return {'status': 'error', 'message': 'No "Date"'}
        if vis_column_num == -1:
            return {'status': 'error', 'message': 'No "Visitors"'}

        req_data['majorDimension'] = 'COLUMNS'
        result = self.service.spreadsheets().values().get(**req_data).execute()
        values = result.get('values', [])
        ma_column_num = self.check_ma_column(ma_column_num, len(values))

        date_column, vis_column = values[date_column_num], values[vis_column_num]
        ma_map = self.calculate_ma(date_column, vis_column)

        # TODO: need find how and replace to bulk insertion
        for key, ma_value in ma_map.iteritems():
            self.insert_cell(self.spr_props.get('sheetId', ''), key, ma_column_num, ma_value)

        return {'status': 'success', 'message': 'Done'}
