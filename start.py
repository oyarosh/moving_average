from __future__ import print_function, absolute_import
import os
import sys
import unittest

from oauth2client.clientsecrets import InvalidClientSecretsError

from ma.moving_average import MovingAverage
from ma.tests import MovingAverageTest


def main():
    base_dir = os.path.dirname(os.path.realpath(__file__))
    if base_dir not in sys.path:
        sys.path.append(base_dir)
        reload(sys)

    # check creds
    try:
        MovingAverage.get_service()
    except InvalidClientSecretsError as e:
        print('''
            Maybe you need to create correct
            Customer ID OAuth.
            Download "client_secret.json"
                FROM https://console.developers.google.com/apis/library
                TO data_robot_test/static/
                (help: http://prntscr.com/dl9q1j and http://prntscr.com/dlb9fh)
            Exception: %s
        ''' % str(e))
        return False

    test_run = raw_input("Run test Y/n (default=n)")
    if test_run and test_run == 'Y':
        suite = unittest.TestLoader().loadTestsFromTestCase(MovingAverageTest)
        unittest.TextTestRunner(verbosity=2).run(suite)

    ssid = raw_input("Please enter spreadsheetID: ")
    ma_period = raw_input("Please enter moving average period(default=3): ")
    try:
        ma_period = int(ma_period)
    except ValueError:
        print('moving average period set as 3')
        ma_period = 3

    result = MovingAverage(ssid=ssid, ma_period=ma_period).process()
    print(result['message'])


if __name__ == '__main__':
    main()
